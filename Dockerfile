#FROM adoptopenjdk/openjdk11:jdk-11.0.2.9-alpine
#FROM alpine:latest

#RUN apk add --no-cache mongodb-tools

FROM mongo

RUN mongorestore --help

COPY verbs.gz /verbs.gz
